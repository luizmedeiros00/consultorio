<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Medical & health landing page templates for online appointment that help you to medical service & patient visit">
        <meta name="author" content="">
        <title>Wealth.life | Medical Landing Page Template</title>
        <!-- Bootstrap Core CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <!-- your favicon icon link -->
        <link rel="shortcut icon" href="image/favicon.ico" type="image/x-icon" />
        
        <!-- Custom CSS -->
        
    </head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    @hasSection('body')
        @yield('body')
    @endif
    
<script src="{{asset('js/jquery-1.10.2.js')}}"></script> 
<script src="{{asset('js/bootstrap.min.js')}}"></script> 
<script src="{{asset('js/jquery.easing.min.js')}}"></script> 
<!-- Custom Theme JavaScript --> 
<script src="{{asset('js/scrolling-nav.js')}}"></script> 
<script src="{{asset('js/owl.carousel.js')}}"></script> 

<script>
    $("#owl-demo").owlCarousel({
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
    });
</script>
</body>
</html>