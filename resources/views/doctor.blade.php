@extends('layout.app')

@section('body')
  @component('layout.nav') 
  @endcomponent
</div>
<!-- intro close -->
<section id="about" class="about-section"><!-- about start -->
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <h1>{{ $medico->nome }}</h1>
        <h1>{{ $medico->especialidade }}</h1>
        <p class="lead">{{ $medico->descricao }}</p>      
      </div>
    </div>
  </div>
</section>
<!-- about close -->
<!-- service close -->
  @component('layout.footer')
  @endcomponent
@endsection
