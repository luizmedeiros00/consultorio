<html>
    <body>
        <h4>Novo Cliente Cadastrado</h4>

        <p>Um novo cliente fez cadastro no sistema.</p>
        <p>Dados do Cliente.</p>
        <ul>
            <li>Nome: {{ $cliente->nome }}</li>
            <li>Telefone: {{ $cliente->telefone }}</li>
            <li>E-mail: {{ $cliente->email }}</li>
        </ul>
    </body>
</html>