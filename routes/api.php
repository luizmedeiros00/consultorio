<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('doctors', 'MedicoController@index')->name('doctors.index');
Route::get('doctors/{id}', 'MedicoController@show')->name('doctors.show');
