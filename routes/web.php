<?php


Route::get('/', function (App\Medico $medicos) {
    $medicos = $medicos->all();
    return view('index', compact('medicos'));
})->name('index');

Route::get('/doctor/{id}', function($id) {
    $medico = App\Medico::findOrFail($id);
    return view('doctor', compact('medico'));
})->name('doctor');

Route::view('/appontment_success', 'appontment_success')->name('appontment_success');

Route::resource('/clientes', 'ClienteController');