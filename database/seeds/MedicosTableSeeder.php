<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MedicosTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('medicos')->insert([
            'nome' => 'Dr. Rodney Stratton',
            'celular' => '(985) 123-3410',
            'especialidade' => 'Physiotherapist',
            'descricao' => 'Sed tristique turpis a libero malesuada, tincidunt elementum mauris euismod.',
            'foto' => 'team-3.png'
        ]);

        DB::table('medicos')->insert([
            'nome' => 'Robert Brown, Prof',
            'celular' => '(985) 231-1234',
            'especialidade' => 'Anesthesiologist',
            'descricao' => 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.',
            'foto' => 'team-2.png'
        ]);

        DB::table('medicos')->insert([
            'nome' => 'Dr. Lita White',
            'celular' => '(985) 231-1234',
            'especialidade' => 'Neurosurgeon',
            'descricao' => 'Maecenas commodo turpis adipiscing, malesuada ipsum in, molestie magna.',
            'foto' => 'team-1.png'
        ]);
    }
}
