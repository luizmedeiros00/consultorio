<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NotificarNovoCliente extends Notification
{
    use Queueable;

    private $cliente;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($cliente)
    {
        $this->cliente = $cliente;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Novo cadastro de cliente no Desctop')
                    ->greeting('Olá')
                    ->line('Esta mensagem é para avisa-lo que um cadastro de um novo cliente foi registrado no sistema')
                    ->line('nome '.$cliente->nome)
                    ->line('e-mail '.$cliente->email)
                    ->line('telefone '.$cliente->telefone)
                    ->salutation("Atenciosamente,\n\nEquipe Catskillet");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
