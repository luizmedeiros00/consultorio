<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Events\CreatedCliente;
use App\Http\Requests\StoreClienteRequest;

class ClienteController extends Controller
{

    public function index()
    {
        
    }

    public function create()
    {
        //
    }

    public function store(StoreClienteRequest $request)
    {
        
        $cliente = Cliente::create($request->all());
        
        if($cliente) {
            event(new CreatedCliente($cliente));
        }
        
        return redirect('/appontment_success');
        
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
