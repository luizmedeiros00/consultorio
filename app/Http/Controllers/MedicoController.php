<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Medico;

class MedicoController extends Controller
{

    public function index()
    {
        $medicos = Medico::all();
        return response()
            ->json($medicos);
    }

    public function show($id)
    {
       $medico = Medico::findOrFail($id);
       return response()
            ->json($medico);
    }
}
