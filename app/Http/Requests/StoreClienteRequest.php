<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreClienteRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required',
            'email' => 'required|email|unique:clientes',
            'telefone' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nome.required' => 'O Nome é obrigatório',
            'email.required'  => 'O E-mail é obrigatório',
            'email.unique'  => 'E-mail já Cadastrado',
            'email.email'  => 'E-mail inválido',
            'telefone.required'  => 'O Eelefone é obrigatório',
        ];
    }
}
