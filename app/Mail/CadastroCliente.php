<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Cliente;

class CadastroCliente extends Mailable
{
    use Queueable, SerializesModels;

    protected $cliente;
 
    public function __construct(Cliente $cliente)
    {
        $this->cliente = $cliente;
    }

    public function build()
    {
        return $this
            ->view('emails.cadastrocliente')
            ->with([
                     'cliente' => $this->cliente
                ]);
    }
}
