<?php

namespace App\Listeners;

use App\Events\CreatedCliente;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\CadastroCliente;

class SendMailCreatedCliente
{

    public function __construct()
    {
        //
    }

    public function handle(CreatedCliente $event)
    {
       
         Mail::to('luizmedeiros00@gmail.com')
                ->send(new CadastroCliente($event->cliente));
         
    }
}
